# Arcade Pi #

This is just a super simple quick hack arcade asteroid shooting game and sprite framework for the Pi and an LED matrix display.

### Ingredients:
 - a Raspberry Pi (Zero is enough)
 - One (or more) 8 x 8 RGB LED matrix (I2C, WS2812/Neoxpixel)
 - A joystick device (4 directions + shooting)
 
Connect everything up, adjust runner.py if needed, and off you shoot.
 
Note: Since I use an analog joystick I needed a ADS1115 AD converter. If you go all digital (e.g. buttons), adjust hal_pi.py.

### 2-Player
Update 7.2.20: Two player mode was added by one of my students. You can now pass joystick 1 or 2 to the human player. Note that
the fire button on the joysticks is now read using digital input pin and its internal pull up resistor, so needs to be connected to gnd.
 
### Run the runner
Run runner.py to make the magic happen. Or asteroids.py (thanks to Tim) for 2-player. Or adjust runner to create your own game. How about Snakes? 
 
### Sprites
It's easy to create new sprites, just check out the framework.py file. Shots are already provided...

