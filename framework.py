import logging
import random
from datetime import time
from time import sleep


class Game:
	def __init__(self, hal, board_x, board_y, player1, player2=None, level=1):
		self.log = logging.getLogger('Game')
		self.board_x = board_x
		self.board_y = board_y
		self.sprites = []
		self.level = level
		self.hal = hal
		self.player1 = player1
		self.sprites.append(player1)
		self.player2 = player2
		if player2 is not None:
			self.sprites.append(player2)
		self.tick_count = 0
		self.score = 0


	def tick(self):
		for s in self.sprites:
			s.tick(self)
			# self.detect_collisions(s)

		self.tick_count += 1
		self.hal.render(self)

	# def detect_collisions(self, s):
	#	pass



class Sprite:
	def __init__(self, start_x, start_y, color=(100, 0, 0), direction=4, speed=0):
		self.log = logging.getLogger('Sprite')
		self.x = start_x
		self.y = start_y
		self.direction = direction
		self.speed = speed
		self.color = color

	def tick(self, game):
		""" The default implementation moves the sprite in the set direction when due. If it moves off bounds it's removed """
		if self.speed > 0 and game.tick_count % self.speed == 0:
			x, y = self.calc_move(self.direction)
			if not self.is_out_of_bounds(game, x, y):
				self.move(x, y)
			else:
				self.log.debug("Sprite {} fell out of the board.".format(self))
				game.sprites.remove(self)


	def calc_move(self, direction, x=None, y=None):
		"""
		Calculate the target coordinates of a move from x, y in the given direction.
		If x or y are not give, the sprite's location is used.
		Return: new x, y (unchecked!)
		"""
		if x is None:
			x = self.x
		if y is None:
			y = self.y

		if direction == 0:
			y -= 1
		elif direction == 1:
			x -= 1
			y -= 1
		elif direction == 2:
			x += 1
		elif direction == 3:
			x += 1
			y += 1
		elif direction == 4:
			y += 1
		elif direction == 5:
			x -= 1
			y += 1
		elif direction == 6:
			x -= 1
		elif direction == 7:
			x -= 1
			y -= 1
		else:
			self.log.error("move(): No valid direction: ", direction)

		return x, y


	def move(self, x, y):
		self.x = x
		self.y = y


	def is_out_of_bounds(self, game, x=None, y=None):
		""" Calculate if the given or the sprite location is outside the board """
		if x is None:
			x = self.x
		if y is None:
			y = self.y

		return x < 0 or x > game.board_x-1 or y < 0 or y > game.board_y-1


	def collision_with(self, game):
		""" Return the first sprite at the same location if found, None otherwise."""

		for s in game.sprites:
			if s != self and self.x == s.x and self.y == s.y:
				return s
		return None


class Human(Sprite):
	def __init__(self, start_x, start_y, color=(200, 0, 50), lifes=3, joystick=1):
		self.log = logging.getLogger('Human')
		self.color = color
		self.orig_color = color
		self.lifes = lifes
		self.joystick = joystick
		super().__init__(start_x, start_y, color)

	def tick(self, game):
		self.color = self.orig_color

		dx, dy = game.hal.move(self.joystick)
		x = self.x + dx
		y = self.y + dy
		#print(dx, dy)

		if not self.is_out_of_bounds(game, x, y):
			self.move(x, y)

		collider = self.collision_with(game)
		if collider is not None:
			# shot removes life.
			if collider.__class__ == Shot:
				self.log.warning("Kollision mit {}".format(collider.__class__.__name__))
				self.lifes -= 1
				self.color = (255, 255, 255)
				if self.lifes < 1:
					self.log.warning("************* PLAYER{} HAT VERLOREN ********".format(self.joystick))
				else:
					self.log.warning("** PLAYER{} HAT NOCH {} LEBEN **".format(self.joystick,self.lifes))
					game.sprites.remove(collider)

			elif collider.__class__ == ExtraLife:
				self.lifes += 1
				self.log.info(" Player{} hat Extraleben erhalten! Leben neu: {}".format(self.joystick,self.lifes))
				game.sprites.remove(collider)

			# star removes life.
			elif collider.__class__ == Star:
				self.log.warning("Kollision mit {}".format(collider.__class__.__name__))
				self.lifes -= 1
				self.color = (255, 255, 255)
				if self.lifes < 1:
					self.log.warning("************* PLAYER{} HAT VERLOREN ********".format(self.joystick))
				else:
					self.log.warning("** PLAYER{} HAT NOCH {} LEBEN **".format(self.joystick ,self.lifes))
					game.sprites.remove(collider)

		#Shooting
		if game.hal.button(joystick=self.joystick):
			dir = 0
			shot_y = 0
			colour_r = 250
			colour_g = 0
			colour_b = 0
			if self.joystick == 1:
				dir = 0
				shot_y = self.y-1
				colour_g = 200
				colour_b = 0
			else:
				dir = 4
				shot_y = self.y+1
				colour_g = 0
				colour_b = 200
			self.log.debug("Päng!")
			shot = Shot(self.x, shot_y, (colour_r, colour_g, colour_b), direction=dir, speed=1)
			game.sprites.append(shot)


class Star(Sprite):
	pass

class ExtraLife(Sprite):
	pass

class Shot(Sprite):

	def tick(self, game):
		# Need to check before and after our move (targets move, too)
		if not self.check_hit(game):
			super().tick(game)
			self.check_hit(game)

	def check_hit(self, game):
		victim = self.collision_with(game)
		if victim is None:
			return False

		if victim.__class__ == Shot:
			game.sprites.remove(victim)
			game.sprites.remove(self)
			return True

		if victim.__class__ == Human:
			pass

		else:
			game.sprites.remove(victim)
			game.sprites.remove(self)
			game.score += 1
			self.log.info("Treffer! Score: {}".format(game.score))
			return True





class RandomWalker(Sprite):
	def __init__(self, start_x, start_y, color=(0, 0, 100)):
		self.log = logging.getLogger('RandomWalker')
		super().__init__(start_x, start_y, color)

	def tick(self, game):
		self.x += random.randint(-1, 1)
		self.y += random.randint(-1, 1)

		if self.x < 0: self.x = 0
		if self.x > game.board_x-1: self.x = game.board_x-1
		if self.y < 0: self.y = 0
		if self.y > game.board_y-1: self.y = game.board_y-1
