import logging
import random
import time

import framework
import hal_pi

logging.basicConfig(level=logging.INFO)

# LED matrix size
cols = 8
rows = 16

hal = hal_pi.HAL_pi(cols, rows)
player = framework.Human(0, rows - 1, (200, 0, 0))
game = framework.Game(hal, cols, rows, player)
# game.sprites.append(framework.RandomWalker(4, 7, color=(0, 200, 0)))
# game.sprites.append(framework.RandomWalker(6, 7, color=(0, 0, 200)))
# game.sprites.append(framework.Star(7, 0, color=(0, 100, 50), direction=4, speed=2))

logging.info("READY PLAYER ONE ...")
time.sleep(2)
star_counter = 0
logging.info("*** LEVEL {} ***".format(game.level))
while True:
	game.tick()
	time.sleep(0.05)

	if player.lifes < 1:
		hal.game_over()
		exit()

	# Add stars
	if game.tick_count % (max(1, 10 - game.level)) == 0:
		max_speed = max(1, 6 - game.level)
		game.sprites.append(framework.Star(random.randint(0, 7), 0, color=(0, 100, 50), direction=4, speed=random.randint(max_speed, 6)))
		star_counter += 1

	# Increase the level
	if game.tick_count % 200 == 0:
		game.level += 1
		logging.info("*** LEVEL {} ***".format(game.level))


	# Pour extra life objects
	if game.tick_count % 200 == 0:
		if random.random() > 0.5:
			x = 0
			d = 2
		else:
			x = 7
			d = 6
		extra_life = framework.ExtraLife(x, random.randint(0, 3), color=(0, 200, 0), direction=d, speed=8)
		game.sprites.append(extra_life)

	# if len(game.sprites) < 2:
	#	game.sprites.append(framework.RandomWalker(random.randint(0, 7), random.randint(0, 7), color=(0, 0, 200)))
