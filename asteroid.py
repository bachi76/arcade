import logging
import random
import time

import framework
import hal_pi

logging.basicConfig(level=logging.INFO)

cols = 8
rows = 16

hal = hal_pi.HAL_pi(cols, rows)
player1 = framework.Human(0, rows - 1, (200, 0, 0), joystick=1)
player2 = framework.Human(cols-1, 0, (0, 0, 200), joystick=2)
game = framework.Game(hal, cols, rows, player1=player1, player2=player2)
# game.sprites.append(framework.RandomWalker(4, 7, color=(0, 200, 0)))
# game.sprites.append(framework.RandomWalker(6, 7, color=(0, 0, 200)))
# game.sprites.append(framework.Star(7, 0, color=(0, 100, 50), direction=4, speed=2))

logging.info("READY PLAYER ONE ...")
time.sleep(2)
star_counter = 0
logging.info("*** LEVEL {} ***".format(game.level))
while True:
	game.tick()
	time.sleep(0.05)

	# Stop the programm when we die
	if player1.lifes < 1 or player2.lifes < 1:
		hal.game_over()
		exit()

	# Add stars

	# left or right?
	random_start_side = random.randint(1,2)
	start_side = 0
	dir = 0
	if random_start_side == 1:
		start_side = 0
		dir = 2
	else:
		start_side = 7
		dir = 6
	if random.random() > 0.95:
		# print("star added")
		star = framework.Star(start_side, random.randint(2, 13), color=(0, 100, 50), direction=dir, speed=3)
		game.sprites.append(star)
		star_counter += 1
