import math
import os
import random


from tabulate import tabulate


class HAL:
	def __init__(self, board_x, board_y):
		self.board_x = board_x
		self.board_y = board_y

	def mov_x(self):
		return 0

	def mov_y(self):
		return 0

	def button(self):
		return 0

	def render(self, game):
		pass

	def game_over(self):
		pass


class HAL_Keyboard(HAL):
	pass


class HAL_Pi(HAL):
	pass

class HAL_Random(HAL):
	def mov_x(self):
		x = random.randint(1, 3)
		if x == 1:
			return -1
		elif x == 2:
			return 0
		else:
			return 1

	def mov_y(self):
		x = random.randint(1, 3)
		if x == 1:
			return -1
		elif x == 2:
			return 0
		else:
			return 1

	def button(self):
		x = random.randint(1, 3)
		if x == 1:
			return -1
		elif x == 2:
			return 0
		else:
			return 1

	def game_over(self):
		pass
