import math
import os
import random
import time

from tabulate import tabulate
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
import neopixel
import board
import busio
import RPi.GPIO as GPIO

i2c = busio.I2C(board.SCL, board.SDA)
ads1115 = ADS.ADS1115(i2c)
ads1115.gain = 1
adc_a0 = AnalogIn(ads1115, ADS.P0)
adc_a1 = AnalogIn(ads1115, ADS.P1)
adc_a2 = AnalogIn(ads1115, ADS.P2)
adc_a3 = AnalogIn(ads1115, ADS.P3)
shoot_pin_player1 = 20
shoot_pin_player2 = 21
GPIO.setup(shoot_pin_player1, GPIO.IN, GPIO.PUD_UP)
GPIO.setup(shoot_pin_player2, GPIO.IN, GPIO.PUD_UP)

class HAL_pi:
	def __init__(self, board_x, board_y):
		self.board_x = board_x
		self.board_y = board_y
		self.neopixel = neopixel.NeoPixel(board.D10, board_x * board_y, brightness=0.1, auto_write=False, pixel_order=neopixel.GRB)

	# Movement
	def move(self, joystick=1):

		#Player 1
		if joystick == 1:
			x_val = adc_a0.value
			y_val = adc_a1.value
		#Player 2
		else:
			x_val = adc_a2.value
			y_val = adc_a3.value


		x_cal = abs(13200 - x_val)
		y_cal = abs(13200 - y_val)

		#print(x_val, y_val)

		# Movement, but only horizontal and vertical
		if x_cal > y_cal:
			if x_val < 13000 * 0.5:
				return -1, 0 #links
			if x_val > 13000 * 1.5:
				return 1, 0 #rechts

			return 0, 0

		else:
			if y_val < 13000 * 0.5:
				return 0, -1 #hoch
			if y_val > 13000 * 1.5:
				return 0, 1 #runter

			return 0, 0

	#Shooting
	def button(self, joystick=1):
		#return adc_a1.value < 2000

		btn = False
		# Set the btn to the right player
		if joystick == 1:
			btn = GPIO.input(shoot_pin_player1)
		elif joystick == 2:
			btn = GPIO.input(shoot_pin_player2)

		if not btn:
			#print("shoot shoot shoot")
			return True
		else:
			#print("dont shoot")
			return False


	def render(self, game):
		self.neopixel.fill((0, 0, 0))
		for s in game.sprites:
			px = s.y * game.board_x + s.x
			self.neopixel[px] = s.color

		self.neopixel.show()

	def game_over(self):
		time.sleep(1)
		for i in range(0, 3):
			self.neopixel.fill((20, 0, 0))
			self.neopixel.show()
			time.sleep(0.1)
			self.neopixel.fill((0, 0, 0))
			self.neopixel.show()
			time.sleep(0.1)
